use std::io;
use rand::Rng;
use std::cmp::Ordering;

fn print_hello()
{
    println!("Hello, world!");
}

fn main() {
    print_hello();

    let secret_val = rand::thread_rng().gen_range(1, 101);
// #if DEBUG
//     println!("Secret val: {}", secret_val);
// #endif

    loop
    {
    let mut val = String::new();
    io::stdin()
    .read_line(& mut val)
    .expect("Failed read line");
    println!("You type: {}", val);
    
    let val: u32 = match val.trim().parse()
    {
        Ok(num) => num,
        Err(_) => continue,
    };

    println!("You type number: {}", val);

    match val.cmp( & secret_val)
    {
        Ordering::Less => println!("Я загадал число побольше"),
        Ordering::Greater => println!("Я загадал число поменьше"),
        Ordering::Equal => 
        {
            println!("Угадал");
            break;
        },       
    }
    }
}
